/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
package utfpr.ct.dainf.if62c.pratica;

public interface FiguraComLados extends Figura{
    public double getLadoMenor();
    public double getLadoMaior();
}
