/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
package utfpr.ct.dainf.if62c.pratica;

public class Quadrado extends Retangulo{
    public Quadrado(double lado){
        super(lado,lado);
    }
    
    public double getLado(){
        return getBase();
    }
    
    @Override
    public String toString() {
        return getNome() + " [" + getLado() + "]";
    }
}
