/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
package utfpr.ct.dainf.if62c.pratica;

public class Retangulo implements FiguraComLados{
    private final double base;
    private final double altura;
    
    public Retangulo(double base, double altura){
        this.base = base;
        this.altura = altura;
    }
    
    public double getBase(){
        return base;
    }
    
    public double getAltura(){
        return altura;
    }
    
    @Override
    public double getLadoMaior(){
        return base > altura ? base : altura;
    }
    
    @Override
    public double getLadoMenor(){
        return base < altura ? base : altura;
    }
    
    @Override
    public double getArea(){
        return base * altura;
    }
    
    @Override
    public double getPerimetro(){
        return base * 2 + altura * 2;
    }
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
    
}
