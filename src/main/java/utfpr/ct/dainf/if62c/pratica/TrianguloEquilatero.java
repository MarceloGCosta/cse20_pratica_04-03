/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
package utfpr.ct.dainf.if62c.pratica;

public class TrianguloEquilatero extends Retangulo{
    public TrianguloEquilatero(double lado){
        super(lado,lado);
    }
    
    public double getLado(){
        return getBase();
    }
    
    @Override
    public double getArea(){
        return super.getArea() / 2;
    }
    
    @Override
    public double getPerimetro(){
        return getBase() * 3;
    }
    
    @Override
    public String toString() {
        return getNome() + " [" + getLado() + "]";
    }
}
