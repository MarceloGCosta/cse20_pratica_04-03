import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
public class Pratica43 {
    public static void main(String[] args) {
        Elipse e = new Elipse(4, 3);
        System.out.println("'"+e.getNome()+"' de eixo 4 e 3");
        System.out.println("Area:" + e.getArea());
        System.out.println("Perimetro:" + e.getPerimetro());

        System.out.println();

        Circulo c = new Circulo(5);
        System.out.println("'"+c.getNome()+"' de raio 5");
        System.out.println("Area:" + c.getArea());
        System.out.println("Perimetro:" + c.getPerimetro());
        
        System.out.println();
        
        Retangulo r = new Retangulo(3, 4);
        System.out.println("'"+r.getNome()+"' de lado 4 e 3");
        System.out.println("Area:" + r.getArea());
        System.out.println("Perimetro:" + r.getPerimetro());

        System.out.println();

        Quadrado q = new Quadrado(1);
        System.out.println("'"+q.getNome()+"' de lado 1");
        System.out.println("Area:" + q.getArea());   
        System.out.println("Perimetro:" + q.getPerimetro());
        
        System.out.println();

        TrianguloEquilatero te = new TrianguloEquilatero(2);
        System.out.println("'"+te.getNome()+"' de lado 2");
        System.out.println("Area:" + te.getArea());   
        System.out.println("Perimetro:" + te.getPerimetro());
    }
}